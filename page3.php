<?php
    session_start();

    define("true_answer", [
        "answer_1" => "a",
        "answer_2" => "b",
        "answer_3" => "c",
        "answer_4" => "d",
        "answer_5" => "a",
        "answer_6" => "b",
        "answer_7" => "c",
        "answer_8" => "d",
        "answer_9" => "a",
        "answer_10" => "b",
    ]);

    $count = 0;

    foreach (true_answer as $key => $answer) {
        if ($answer == $_SESSION[$key])
            $count++;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

    <div class="wrapper">
        <h2 style="text-align: center;">Kết quả</h2>
        <?php

        if (true) {
            echo "Bạn làm đúng " . $count . " câu " ."</br>";
        }
        if ($count < 4) {
            echo "Bạn quá kém, cần ôn tập thêm";
        }
        elseif ($count < 7) {
            echo "Cũng bình thường";
        }
        else {
            echo "Sắp sửa làm được trợ giảng lớp PHP";
        }
        ?>

    </div>
</body>
</html>