<?php
    session_start();

    $questions = [
        "Câu hỏi 1" => ["A1","B1","C1","D1"],
        "Câu hỏi 2" => ["A2","B2","C2","D2"],
        "Câu hỏi 3" => ["A3","B3","C3","D3"],
        "Câu hỏi 4" => ["A4","B4","C4","D4"],
        "Câu hỏi 5" => ["A5","B5","C5","D5"]
    ];
    $answer_labels = ["a", "b", "c", "d"];

    if (isset($_POST['submit'])) {
        for ($i = 1; $i <= 5; $i++) {
            if (isset($_POST['answer_' . $i])) {
                $_SESSION['answer_' . $i] = $_POST['answer_' . $i];
            }
            else {
                $_SESSION['answer_' . $i] = "e";
            }
        }
        header('Location: '.'page2.php');
    }

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div class="wrapper">
        <form action="" method="post">
            <h2 style="text-align:center ;">Trắc nghiệm trang 1</h2>
                <?php
                $count = 1;
                foreach ($questions as $question => $answers) {
                ?>
                    <div class="question-box">
                        <div class="question-filed"><?php echo "Question " . $count . ": " . $question ?></div>
                        <?php
                        for ($i = 0; $i < sizeof($answers); $i++) {
                        ?>
                            <div class="answer-field">
                                <input type="radio" name=<?php echo "answer_" . $count ?> value=<?php echo $answer_labels[$i] ?>  id=<?php echo "answer_" . $count . $answer_labels[$i]; ?>>
                                <label for=<?php echo "answer_" . $count . $answer_labels[$i]; ?>><?php echo $answers[$i] ?></label>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                <?php
                $count++;
                }
                ?>
                <button name="submit" id="button-submit" class="button-submit" type="submit">Next</button>
        </form> 
    </div>
</body>
</html>